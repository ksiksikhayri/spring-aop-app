package tn.esprit.spring.ioc.app.boot;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import tn.esprit.spring.ioc.app.services.AuthenticationAgent;

public class Main {

	public static void main(String[] args) {
		ApplicationContext context = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		AuthenticationAgent agent = context.getBean(AuthenticationAgent.class);
		System.out.println(agent.authenticate("admin", "adminpass") ? "Authentification r�ussie" : "Authentification �chou�e");
	}

}

package tn.esprit.spring.ioc.app.contracts;

import java.util.List;

import tn.esprit.spring.ioc.app.entities.User;

public interface AuthenticationProvider {

	void addUser(User user);

	List<User> getUsers();

	void setUsers(List<User> users);
}

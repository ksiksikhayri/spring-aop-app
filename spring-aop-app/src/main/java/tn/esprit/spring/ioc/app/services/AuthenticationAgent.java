package tn.esprit.spring.ioc.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import tn.esprit.spring.ioc.app.contracts.AuthenticationProvider;
import tn.esprit.spring.ioc.app.contracts.Authenticator;
import tn.esprit.spring.ioc.app.entities.User;

@Component
public class AuthenticationAgent implements Authenticator {

	@Autowired
	@Qualifier("mock")
	private AuthenticationProvider authenticationProvider;

	@Override
	public boolean authenticate(String username, String password) {
		for (User u : authenticationProvider.getUsers()) {
			if (u.getUsername().equals(username) && u.getPassword().equals(password)) {
				return true;
			}
		}
		return false;
	}

	public AuthenticationProvider getAuthenticationProvider() {
		return authenticationProvider;
	}

	public void setAuthenticationProvider(AuthenticationProvider authenticationProvider) {
		this.authenticationProvider = authenticationProvider;
	}
	
	

}

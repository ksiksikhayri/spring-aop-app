package tn.esprit.spring.ioc.app.services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import tn.esprit.spring.ioc.app.entities.Role;
import tn.esprit.spring.ioc.app.entities.User;

@Component
@Qualifier("mock")
public class MockAuthenticationProvider extends GenericAuthenticationProvider {
	

	@Override
	public void addUser(User user) {
		users.add(user);
	}

	@Override
	public List<User> getUsers() {
		return users;
	}

	@Override
	public void setUsers(List<User> users) {
		this.users = users;
	}

	@PostConstruct
	public void init(){
		User admin = new User("admin", "adminpass");
		User user = new User("user", "userpass");
		Role readRole = new Role("read");
		Role writeRole = new Role("write");
		List<Role> adminRoles = new ArrayList<>();
		adminRoles.add(readRole);
		adminRoles.add(writeRole);
		List<Role> userRoles = new ArrayList<>();
		userRoles.add(readRole);
		user.setRoles(userRoles);
		admin.setRoles(adminRoles);
		users = new ArrayList<>();
		users.add(user);
		users.add(admin);
	}

}

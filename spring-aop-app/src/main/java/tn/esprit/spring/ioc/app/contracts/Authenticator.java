package tn.esprit.spring.ioc.app.contracts;

public interface Authenticator {

	boolean authenticate(String username, String password);
}

package tn.esprit.spring.ioc.app.entities;

public class Role {

	private String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Role(String label) {
		super();
		this.label = label;
	}

}
